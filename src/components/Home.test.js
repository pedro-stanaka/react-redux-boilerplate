import React from 'react';
import {mount, shallow} from "enzyme";

import Home from "./Home";

it('renders without breaking', () => {
    const wrapper = shallow(<Home/>);
});

it('contains a title', () => {
  const wrapper = mount(<Home/>);
  expect(wrapper.find('.App-title')).toHaveText('Welcome to React');
});
