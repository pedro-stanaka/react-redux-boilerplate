import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
// Add jest-enzyme easy matchers
import 'jest-enzyme';

configure({ adapter: new Adapter() });
