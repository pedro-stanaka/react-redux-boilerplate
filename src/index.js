import React from 'react';
import ReactDom from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, dispatch} from "redux";
import promiseMiddleware from "redux-promise";
import reduxThunk from 'redux-thunk';
import reducers from './reducers';

import App from "./App";

const createStoreWithMiddleware = applyMiddleware(promiseMiddleware, reduxThunk)(createStore);

const improvedStore = createStoreWithMiddleware(reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDom.render(
    <Provider store={improvedStore}>
        <App />
    </Provider>,
    document.getElementById('root')
);