import React from 'react';

const NotFound = () => {
    return (
        <div className="text-center">
            <h1>Oops!</h1>
            <h2>That page cannot be found!</h2>
        </div>
    );
};

export default NotFound;